export const data = [
  {
    id: 1,
    name: 'Doretta',
    email: 'dkiendl0@msu.edu',
    lvl: '10',
    experience: '999',
  },
  {
    id: 2,
    name: 'Annabal',
    email: 'apaver1@ft.com',
    lvl: '99',
    experience: '351',
  },
  {
    id: 3,
    name: 'Cathlene',
    email: 'cjenton2@networkadvertising.org',
    lvl: '55',
    experience: '1000',
  },
];
