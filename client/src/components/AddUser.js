import React, { useState, useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import { v4 as uuid } from 'uuid';
import { Link, useHistory } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

export const AddUser = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [lvl, setLvl] = useState('');
  const [experience, setExperiance] = useState('');

  const { addUser } = useContext(GlobalContext);
  const history = useHistory();

  // onSubmit
  const onSubmit = (e) => {
    e.preventDefault();
    const newUser = {
      id: uuid(),
      name,
      email,
      lvl,
      experience,
    };
    addUser(newUser);
    history.push('/');
  };

  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        <Label>Username</Label>
        <Input type="text" value={name} onChange={(e) => setName(e.target.value)} name="name" placeholder="Enter a username" required></Input>

        <Label>Email</Label>
        <Input type="text" value={email} onChange={(e) => setEmail(e.target.value)} name="email" placeholder="Enter an email" required></Input>

        <Label>Lvl</Label>
        <Input type="text" value={lvl} onChange={(e) => setLvl(e.target.value)} name="lvl" placeholder="Enter an email" required></Input>

        <Label>Experience</Label>
        <Input type="text" value={experience} onChange={(e) => setExperiance(e.target.value)} name="experience" placeholder="Enter an email" required></Input>
      </FormGroup>

      <Button type="submit">Submit</Button>
      <Link to="/" className="btn btn-danger ml-2">
        Cancel
      </Link>
    </Form>
  );
};
